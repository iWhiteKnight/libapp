enum Genre {
    Comedy,
    Drama,
    Horror,
    Documentary,
    Romance,
    Satire,
    Tragedy,
    Tragicomedy,
    Fantasy,
    Mythology,
    Adventure
}

export default Genre;