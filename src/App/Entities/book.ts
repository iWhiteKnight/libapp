import Author from "./author";
import Issuer from "./issuer";
import Genre from "./genre";

export default class Book {
    isbn: String;
    name: String;
    issueDate: Date;
    authors: Author[];
    issuer: Issuer;
    genres: Genre[];
    coverImage: String;
}