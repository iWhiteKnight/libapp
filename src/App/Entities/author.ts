export default class Author{
    firstName: String;
    lastName: String;
    middleName: String;
    birthDate: Date;
    deathDate: Date;
    nationality: String;
}