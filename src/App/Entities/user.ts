export default class User {
    id: String;
    firstName: String;
    lastName: String;
    email: String;
}