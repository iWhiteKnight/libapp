export default class Issuer {
    name: String;
    establishDate: Date;
    country: String;
}